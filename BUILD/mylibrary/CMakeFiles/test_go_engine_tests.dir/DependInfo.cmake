# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/game/Game_Design/go_game/mylibrary/tests/go_engine_tests.cpp" "E:/game/Game_Design/go_game/BUILD/mylibrary/CMakeFiles/test_go_engine_tests.dir/tests/go_engine_tests.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "E:/game/Game_Design/go_game/BUILD/mylibrary/CMakeFiles/mylibrary.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "E:/game/Game_Design/gtest-1.7.0/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
