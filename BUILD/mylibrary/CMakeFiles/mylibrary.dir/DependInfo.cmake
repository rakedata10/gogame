# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "E:/game/Game_Design/go_game/mylibrary/go.cpp" "E:/game/Game_Design/go_game/BUILD/mylibrary/CMakeFiles/mylibrary.dir/go.cpp.obj"
  "E:/game/Game_Design/go_game/mylibrary/player.cpp" "E:/game/Game_Design/go_game/BUILD/mylibrary/CMakeFiles/mylibrary.dir/player.cpp.obj"
  "E:/game/Game_Design/go_game/mylibrary/randomai.cpp" "E:/game/Game_Design/go_game/BUILD/mylibrary/CMakeFiles/mylibrary.dir/randomai.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "E:/game/Game_Design/gtest-1.7.0/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
