# CMake generated Testfile for 
# Source directory: E:/game/Game_Design/go_game/mylibrary
# Build directory: E:/game/Game_Design/go_game/BUILD/mylibrary
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(GoEngineTest_Board.FromStateConstructor "test_go_engine_tests" "--gtest_filter=GoEngineTest_Board.FromStateConstructor")
add_test(GoEngineTest_LegalStonePlacements.SimpleKoRuleTest "test_go_engine_tests" "--gtest_filter=GoEngineTest_LegalStonePlacements.SimpleKoRuleTest")
